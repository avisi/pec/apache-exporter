FROM registry.gitlab.com/avisi/base/centos:7 as build

ARG APACHE_EXPORTER_VERSION=0.5.0
ARG APACHE_EXPORTER_SHA256=60dc120e0c5d9325beaec4289d719e3b05179531f470f87d610dc2870c118144

USER root
RUN curl -L -O https://github.com/Lusitaniae/apache_exporter/releases/download/v${APACHE_EXPORTER_VERSION}/apache_exporter-${APACHE_EXPORTER_VERSION}.linux-amd64.tar.gz \
    && echo "${APACHE_EXPORTER_SHA256} apache_exporter-${APACHE_EXPORTER_VERSION}.linux-amd64.tar.gz" | sha256sum -c \
    && tar -zxvf apache_exporter-${APACHE_EXPORTER_VERSION}.linux-amd64.tar.gz \
    && mv apache_exporter-0.5.0.linux-amd64/apache_exporter /usr/local/bin/apache_exporter \
    && rm apache_exporter-${APACHE_EXPORTER_VERSION}.linux-amd64.tar.gz

FROM scratch 

EXPOSE 9117/tcp

COPY --from=build /usr/local/bin/apache_exporter /usr/local/bin/apache_exporter

CMD ["/usr/local/bin/apache_exporter"]
