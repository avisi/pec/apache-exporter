# Apache Prometheus Exporter

[![pipeline status](https://gitlab.com/avisi/pec/apache-exporter/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/apache-exporter/commits/master)

> HTTPD Prometheus metrics exporter in a Docker image

This repository contains a Dockerfile with a Prometheus Apache Exporter installation.

## Table of Contents

- [Usage](#usage)
- [Contribute](#contribute)
- [Automated build](#automated build)
- [License](#license)

## Usage

Start the `registry.gitlab.com/avisi/pec/apache-exporter:0.5.0` image and point it to an httpd server-status endpoint.

Example for in a pod:

```yaml
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: httpd
  namespace: default
  labels:
    app: httpd
spec:
  replicas: 1
  selector:
    matchLabels:
      app: httpd
  template:
    metadata:
      labels:
        app: httpd
    spec:
      containers:
        - name: metrics
          image: registry.gitlab.com/avisi/pec/apache-exporter:0.5.0
          imagePullPolicy: IfNotPresent
          ports:
          - name: metrics
            containerPort: 9117
        - name: httpd
          image: httpd
          imagePullPolicy: IfNotPresent
          ports:
          - containerPort: 80
            name: http
```

## Automated build

This image is build at least once a month automatically. All PR's are automatically build.

## Contribute

PRs accepted. All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/apache-exporter/issues).

## License

[MIT © Avisi B.V.](LICENSE)
